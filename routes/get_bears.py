from typing import List

import requests
from dacite import from_dict

from config import Config
from utils.bear import Bear


def get_bears() -> List[Bear]:
    r = requests.get(f"{Config.base_url}/bear", data=locals())
    bears = []
    for bear in r.json():
        bears.append(from_dict(data_class=Bear, data=bear))
    return bears
