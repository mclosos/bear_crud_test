import requests

from config import Config


def delete_bears():
    requests.delete(f"{Config.base_url}/bear")
