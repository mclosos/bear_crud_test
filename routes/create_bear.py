import requests

from config import Config
from utils.bear_type import BearType


def create_bear(bear_type: BearType, bear_name: str, bear_age: float) -> str:
    data = {'bear_type': bear_type, 'bear_name': bear_name, 'bear_age': bear_age}
    r = requests.post(f"{Config.base_url}/bear", json=data)
    bear_id = r.text
    return bear_id
