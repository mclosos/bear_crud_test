import requests

from config import Config


def delete_bear(bear_id):
    requests.delete(f"{Config.base_url}/bear/{bear_id}")
