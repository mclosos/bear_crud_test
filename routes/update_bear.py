import requests

from config import Config
from utils.bear_type import BearType


def update_bear(bear_id, bear_type: BearType = None,
                bear_name: str = None, bear_age: float = None) -> str:
    data = {k: v for k, v in locals().items()
            if v not in ('None', None, 'bear_id')}
    data.pop('bear_id', None)
    r = requests.put(f"{Config.base_url}/bear/{bear_id}", json=data)
    return r.text
