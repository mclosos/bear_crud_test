from typing import Any

import requests
from dacite import from_dict

from config import Config
from utils.bear import Bear


def get_bear(bear_id) -> Any:
    r = requests.get(f"{Config.base_url}/bear/{bear_id}", data=locals())
    if r.text == "EMPTY":
        return r.text
    else:
        return from_dict(data_class=Bear, data=r.json())
