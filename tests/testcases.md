
----
## Testcase #1. Get empty list of bears

### Precondition:
...

### Step:
`curl http://localhost:8091/bear`

### Expected result:
`200, []`

----

## Testcase #2. Create bear

### Precondition:
...

### Step 1:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`

### Expected result:
`200, 1 \n OK`

### Step 2:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`

### Expected result:
`200, 2 \n OK`

----

## Testcase #3. Get list of bears

### Precondition:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`

### Steps:
`curl http://localhost:8091/bear`

### Expected result:
`200, [{"bear_id":1,"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5},{"bear_id":2,"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}]`

----

## Testcase #4. Update bear

### Precondition:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`
`1`
### Step 1:
`curl -X PUT -d '{"bear_age":5}' http://localhost:8091/bear/1`

### Expected result:
`200, 1 \n OK`

### Step 2:
`curl http://localhost:8091/bear/1`

### Expected result:
`200, {"bear_id":1,"bear_type":"BLACK","bear_name":"mikhail","bear_age":5}`

----

## Testcase #5. Handle create bear with insufficient parameters

### Precondition:
...

### Step:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail"}' http://localhost:8091/bear`

### Expected result:
`200, Error. Pls fill all parameters`

----

## Testcase #6. Delete bear

### Precondition:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`

### Step 1:
`curl -X DELETE http://localhost:8091/bear/1`

### Expected result:
`200, OK `

### Step 2:
`curl http://localhost:8091/bear`

### Expected result:
`200, [] `

----

## Testcase #7. Delete all bears

### Precondition:
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`
`curl -d '{"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}' http://localhost:8091/bear`

### Step:
`curl -X DELETE http://localhost:8091/bear`

### Expected result:
`200, OK`

### Step 2:
`curl http://localhost:8091/bear`

### Expected result:
`200, [] `

----