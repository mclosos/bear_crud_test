import pytest

from routes.create_bear import create_bear
from routes.get_bear import get_bear
from tests.test_base import TestBase
from utils.bear import Bear
from utils.bear_type import BearType


@pytest.mark.parametrize('bear_type,bear_name,bear_age',
                         [(BearType.black, "mikhail", 12.0),
                          (BearType.polar, "joe", 0.5),
                          (BearType.brown, "doe", 19.9)])
class TestCreateBear(TestBase):
    def test_create_bear(self, bear_type, bear_name, bear_age):
        bear_id = create_bear(bear_type, bear_name, bear_age)
        bear = get_bear(bear_id)
        assert isinstance(bear, Bear)
        assert bear.bear_type == bear_type
        assert bear.bear_name.lower() == bear_name
        assert bear.bear_age == bear_age
