from utils.cleanup import cleanup


class TestBase:
    @classmethod
    def teardown_class(cls):
        cleanup()
