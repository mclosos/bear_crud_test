from routes.create_bear import create_bear
from routes.delete_bear import delete_bear
from routes.delete_bears import delete_bears
from routes.get_bear import get_bear
from routes.get_bears import get_bears
from tests.test_base import TestBase
from utils.bear_type import BearType


class TestDeleteBear(TestBase):
    @classmethod
    def setup_class(cls):
        cls.bear_id = create_bear(BearType.brown, "Andrey", 35.0)

    def test_delete_bear(self):
        delete_bear(self.bear_id)
        bear = get_bear(self.bear_id)
        assert bear == "EMPTY"

    def setup_method(self, test_delete_all_bears):
        create_bear(BearType.polar, "Bro", 12.0)
        create_bear(BearType.gummy, "George", 3.0)

    def test_delete_all_bears(self):
        delete_bears()
        assert get_bears() == []
