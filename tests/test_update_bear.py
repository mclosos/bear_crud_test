from routes.create_bear import create_bear
from routes.get_bear import get_bear
from routes.update_bear import update_bear
from tests.test_base import TestBase
from utils.bear_type import BearType


class TestUpdateBear(TestBase):
    @classmethod
    def setup_class(cls):
        cls.bear_id = create_bear(BearType.brown, "Andrey", 35.0)

    def test_update_bear(self):
        update_bear(bear_id=self.bear_id, bear_name="Boba")
        bear = get_bear(self.bear_id)
        assert bear.bear_name == "Boba"
